//
//  ParserHelper.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import Foundation

protocol Parceable {
    static func parseObject(dashboardInfo: StudentDashboard) -> Result<Self, ErrorResult>
}
//Service parser helper.
final class ParserHelper {
    static func parse<T: Parceable>(data: Data, completion : (Result<T, ErrorResult>) -> Void) {
        
        do {
            let jsonDecoder = JSONDecoder()
            let dashboardInfo = try jsonDecoder.decode(StudentDashboard.self, from: data)
                switch T.parseObject(dashboardInfo: dashboardInfo) {
                case .failure(let error):
                    completion(.failure(error))
                    break
                case .success(let newModel):
                    completion(.success(newModel))
                    break
                }
        } catch {
            // can't parse json
            completion(.failure(.parser(string: "Error while parsing json data")))
        }
    }
}
