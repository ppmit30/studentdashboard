//
//  Result.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import Foundation

/**
    Enum to identify success or failure of service.
 */
enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}
