//
//  JSONService.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 15/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import Foundation

/**
    Enum will represent service error types
 */
enum JSONError: String, Error {
    case noNetwork = "No Network"
    case serverOverload = "Server is overloaded"
    case permissionDenied = "You don't have permission"
}

/**
    FileTypes Represents extention of persistance files.
 */
enum FileTypes: String {
    case json
    case plist
}

/// Prototcol oriented service type
protocol JSONServiceProtocol: class {
    func fetchStudentSchedule(_ completion: @escaping ((Result<StudentDashboard, ErrorResult>) -> Void))
}

final class FileDataService: JSONServiceProtocol {
    static let shared = FileDataService()
    private let studentDashboardJsonFile = "StudentDashboard"
    
    /// FileManager will asign path.
    /// - Parameter completion: Completion will keep the reference of expected result or any error ncounterred during process.
    func fetchStudentSchedule(_ completion: @escaping ((Result<StudentDashboard, ErrorResult>) -> Void)) {
        guard let data = FileManager.readJsonData(forResource: studentDashboardJsonFile) else {
            completion(Result.failure(ErrorResult.custom(string: "No file or data")))
            return
        }
        
        ParserHelper.parse(data: data, completion: completion)
    }
}

extension FileManager {
    
    /// File Reading
    /// - Parameter fileName: Passing argument as file name and returning as raw data
    static func readJsonData(forResource fileName: String ) -> Data? {
        let bundle = Bundle(for: FileDataService.self)
        if let path = bundle.path(forResource: fileName, ofType: FileTypes.json.rawValue) {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch {
                print("Parsing error \(error.localizedDescription)")
            }
        }
        return nil
    }
}
