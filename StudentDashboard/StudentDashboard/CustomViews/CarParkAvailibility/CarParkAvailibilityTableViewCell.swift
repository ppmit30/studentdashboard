//
//  CarParkAvailibilityTableViewCell.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import UIKit

class CarParkAvailibilityTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet weak var parkingAreaLbl: UILabel!
    @IBOutlet weak var parkingLotAvailableLbl: UILabel!
    @IBOutlet weak var parkingBackgroundView: UIView!
    
    @IBOutlet weak var parkingBackgroundViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var parkingBackgroundViewBottomConstraint: NSLayoutConstraint!
    
    /// Cell update with parking availibility
    /// - Parameter carPark: Car park param describe availibility and location.
    public func updateCarSpaceAvailibility(_ carPark: CarParkAvailibility) {
        self.parkingAreaLbl.text = carPark.locationName
        self.parkingLotAvailableLbl.text = "\(carPark.available)"
    }
    
    
    func addShadowToCellInTableView(_ tableView: UITableView, at indexPath: IndexPath) {
        
        parkingBackgroundViewTopConstraint.constant = 0
        parkingBackgroundViewBottomConstraint.constant = 0
        
        let isFirstRow = (indexPath.row == 0)
        let isLastRow = (indexPath.row == (tableView.numberOfRows(inSection: indexPath.section) - 1))

        if isFirstRow && isLastRow {
            parkingBackgroundViewTopConstraint.constant = 5
            parkingBackgroundViewBottomConstraint.constant = 5
            parkingBackgroundView.layer.cornerRadius = 3
            parkingBackgroundView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else if isFirstRow {
            parkingBackgroundViewTopConstraint.constant = 5
            parkingBackgroundView.layer.cornerRadius = 3
            parkingBackgroundView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else if isLastRow {
            parkingBackgroundViewBottomConstraint.constant = 5
            parkingBackgroundView.layer.cornerRadius = 3
            parkingBackgroundView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        }
        
        parkingBackgroundView.layer.shadowColor = UIColor.lightGray.cgColor
        parkingBackgroundView.layer.shadowOpacity = 1
        parkingBackgroundView.layer.shadowOffset = CGSize.zero
        parkingBackgroundView.layer.shadowRadius = 3
    }
}
