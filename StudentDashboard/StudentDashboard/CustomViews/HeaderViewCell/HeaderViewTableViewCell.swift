//
//  HeaderViewTableViewCell.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import UIKit

class HeaderViewTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var sectionTitleLbl: UILabel!
    
    public func setSectionName(_ sectionTitle: InfoSection) {
        sectionTitleLbl.text = sectionTitle.sectionName
    }
}
