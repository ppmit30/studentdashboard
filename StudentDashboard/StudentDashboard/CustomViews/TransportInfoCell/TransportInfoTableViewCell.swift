//
//  TransportInfoTableViewCell.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import UIKit

class TransportInfoTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var pickupLocationLbl: UILabel!
    @IBOutlet weak var dropOffLocationLbl: UILabel!
    @IBOutlet weak var waitingTimeLbl: UILabel!
    @IBOutlet weak var customBackgroundView: UIView!
    
    @IBOutlet weak var backgroundViewTopContstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundViewBottomConstraint: NSLayoutConstraint!
    /// Campus transport description
    /// - Parameter campusTransport: Parameter describe campus transport with time and location
    public func updateCellWithCampusTransportDetails(_ campusTransport: InterCampusBus) {
        self.pickupLocationLbl.text = campusTransport.pickupPoint
        self.dropOffLocationLbl.text = campusTransport.dropOffPoint
        self.waitingTimeLbl.text = campusTransport.waitingTime
    }
    
    func addShadowToCellInTableView(_ tableView: UITableView, at indexPath: IndexPath) {
        
        backgroundViewTopContstraint.constant = 0
        backgroundViewBottomConstraint.constant = 0
        
        let isFirstRow = (indexPath.row == 0)
        let isLastRow = (indexPath.row == (tableView.numberOfRows(inSection: indexPath.section) - 1))

        if isFirstRow && isLastRow {
            backgroundViewTopContstraint.constant = 5
            backgroundViewBottomConstraint.constant = 5
            customBackgroundView.layer.cornerRadius = 3
            customBackgroundView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else if isFirstRow {
            backgroundViewTopContstraint.constant = 5
            customBackgroundView.layer.cornerRadius = 3
            customBackgroundView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else if isLastRow {
            backgroundViewBottomConstraint.constant = 5
            customBackgroundView.layer.cornerRadius = 3
            customBackgroundView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        }
        
        customBackgroundView.layer.shadowColor = UIColor.lightGray.cgColor
        customBackgroundView.layer.shadowOpacity = 1
        customBackgroundView.layer.shadowOffset = CGSize.zero
        customBackgroundView.layer.shadowRadius = 3
    }
}
