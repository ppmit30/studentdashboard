//
//  LectureInfoTableViewCell.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import UIKit

class LectureInfoTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var lectureStartTimeLbl: UILabel!
    @IBOutlet weak var lectureEndTimeLbl: UILabel!
    @IBOutlet weak var lectureNameLbl: UILabel!
    @IBOutlet weak var lecturerNameLbl: UILabel!
    @IBOutlet weak var lectureLocationLbl: UILabel!
    @IBOutlet weak var dayTitleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var todayLbl: UILabelX!
    @IBOutlet weak var todayViewBtn: UIButton!
    
    @IBOutlet weak var customBackgroundView: UIView!
    
    @IBOutlet weak var backgroundViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundViewBottomConstraint: NSLayoutConstraint!
    /// Student Lecture Information
    /// - Parameters:
    ///   - model: LectureInfo Param describe students classes detail
    ///   - cellNumber: specify which cell will be displayed.
    public func getLectureInfo(_ classesInfo: LectureInfo,And cellNumber: Int) {
        if cellNumber == 0 {
            self.todayView.isHidden = false
            self.dayTitleHeightConstraint.constant = 25
            self.todayLbl.customiseCornerRadius([.bottomRight], 5)
        } else {
            self.todayView.isHidden = true
            self.dayTitleHeightConstraint.constant = 0
        }
        
        self.lectureStartTimeLbl.text = classesInfo.startTime
        self.lectureEndTimeLbl.text = classesInfo.finishTime
        self.lectureNameLbl.text = classesInfo.lectureName
        self.lecturerNameLbl.text = classesInfo.lecturerName
        self.lectureLocationLbl.text = classesInfo.lectureLocation
    }
    
    func addShadowToCellInTableView(_ tableView: UITableView, at indexPath: IndexPath) {
                        
        backgroundViewTopConstraint.constant = 0
        backgroundViewBottomConstraint.constant = 0
        
        let isFirstRow = (indexPath.row == 0)
        let isLastRow = (indexPath.row == (tableView.numberOfRows(inSection: indexPath.section) - 1))

        if isFirstRow && isLastRow {
            backgroundViewTopConstraint.constant = 5
            backgroundViewBottomConstraint.constant = 5
            customBackgroundView.layer.cornerRadius = 3
            customBackgroundView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else if isFirstRow {
            backgroundViewTopConstraint.constant = 5
            customBackgroundView.layer.cornerRadius = 3
            customBackgroundView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else if isLastRow {
            backgroundViewBottomConstraint.constant = 5
            customBackgroundView.layer.cornerRadius = 3
            customBackgroundView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        }
        
        customBackgroundView.layer.shadowColor = UIColor.lightGray.cgColor
        customBackgroundView.layer.shadowOpacity = 1
        customBackgroundView.layer.shadowOffset = CGSize.zero
        customBackgroundView.layer.shadowRadius = 3
        
        todayLbl.layer.cornerRadius = 3
        todayLbl.layer.maskedCorners = [.layerMinXMinYCorner]
    }
    
}
