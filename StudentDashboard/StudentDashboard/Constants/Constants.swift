//
//  Constants.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import UIKit

/// All static value asignemnt
final class Constants {
    public static let transportInfoCellIdentifier = "TransportInfoCellIdentifier"
    public static let lectureInfoCellIdentifier = "LectureInfoCellIdentifier"
    public static let sectionHeaderIdentifier = "sectionHeaderIdentifier"
    public static let carParkAvailibilityIdentifier = "CarParkAvailibilityIdentifier"
    public static let lectureInfoCellHeight = 130
    public static let lectureInfoCellWithoutView = 105
    public static let regulareCellHeight = 55
    public static let heightForHeaderInSection = 30
    public static let heightForFooterInSection = 10
}
