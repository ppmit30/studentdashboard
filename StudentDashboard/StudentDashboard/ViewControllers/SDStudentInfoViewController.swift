//
//  SDStudentInfoViewController.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 15/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import UIKit

class SDStudentInfoViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var studentDashboardTableView: UITableView!
    
    // MARK: - Instant Properties
    let dataSource = DashboardDataSource()
    lazy var viewModel : StudentDashboardViewModel = {
        let viewModel = StudentDashboardViewModel(dataSource: dataSource)
        return viewModel
    }()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        customCellRegistration()
        self.studentDashboardTableView.tableFooterView = UIView()
        
        //Service call through viewModel.
        self.viewModel.fetchStudentCampusInfo()

        self.studentDashboardTableView.delegate = self.dataSource
        self.studentDashboardTableView.dataSource = self.dataSource
        
        self.datasourceNotification()
                
        // add error handling example
        self.viewModel.onErrorHandling = { [weak self] error in
            // display error ?
            let controller = UIAlertController(title: "An error occured", message: "Oops, something went wrong!", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
            self?.present(controller, animated: true, completion: nil)
        }
    }
    
    private func datasourceNotification() {
        self.dataSource.data.addAndNotify(observer: self) { [weak self] _ in
            guard let self = self else { return }
            //Reload tableview
            self.studentDashboardTableView.reloadData()
            
            //Update navigation bar
            let studentName = (self.dataSource.data.value.first?.studentName ?? "")
            let studentSchedule =  (self.dataSource.data.value.first?.schedule ?? "")
            self.setNavigationBarItems(studentName, And: studentSchedule)
        }
    }
    
    // MARK: Custom Cell Registration
    private func customCellRegistration() -> Void {
        studentDashboardTableView.register(UINib(nibName: "HeaderViewTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.sectionHeaderIdentifier)
        studentDashboardTableView.register(UINib(nibName: "CarParkAvailibilityTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.carParkAvailibilityIdentifier)
        studentDashboardTableView.register(UINib(nibName: "LectureInfoTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.lectureInfoCellIdentifier)
        studentDashboardTableView.register(UINib(nibName: "TransportInfoTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.transportInfoCellIdentifier)
    }
    
    
    /// Custom Navigation Bar
    /// - Parameters:
    ///   - name: Student Name
    ///   - schedule: Student Schedule week
    private func setNavigationBarItems(_ studentName:String, And schedule: String) {
        let userButton = UIButton(type: .system)
        userButton.setImage(#imageLiteral(resourceName: "profile"), for: .normal)
        userButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        
        let bellButton = UIButton(type: .system)
        let bellIconImage = UIImage(systemName: "bell")
        bellButton.setImage(bellIconImage, for: .normal)
        bellButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: userButton),UIBarButtonItem(customView: bellButton)]
        
        if let navigationBar = self.navigationController?.navigationBar {
            let titleFrame = CGRect(x: 15, y: 0, width: navigationBar.frame.width/2, height: 21)
            let scheduleFrame = CGRect(x: 15, y: 21, width: navigationBar.frame.width/2, height: 21)
            
            let studentNameLbl = UILabel(frame: titleFrame)
            studentNameLbl.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
            studentNameLbl.text = studentName
            
            let studentWeek = UILabel(frame: scheduleFrame)
            studentWeek.font = UIFont(name: "HelveticaNeue", size: 15)
            studentWeek.textColor = .darkGray
            studentWeek.text = schedule
            
            navigationBar.addSubview(studentNameLbl)
            navigationBar.addSubview(studentWeek)
        }
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
}
