//
//  Dashboard.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 15/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import UIKit

/**
 This is your dashboard information
 */
public struct StudentDashboard: Codable {
    var lectureInfo: [LectureInfo]
    var carParkAvailibility: [CarParkAvailibility]
    var interCampusBus: [InterCampusBus]
    var infoSection: [InfoSection]
    var studentName: String
    var schedule: String
}

extension StudentDashboard : Parceable {
    /// This will parse dashboard information and return result.
    static func parseObject(dashboardInfo: StudentDashboard) -> Result<StudentDashboard, ErrorResult> {
        return Result.success(dashboardInfo)
    }
}
