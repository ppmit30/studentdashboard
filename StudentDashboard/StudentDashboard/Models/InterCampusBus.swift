//
//  InterCampusBus.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 15/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import Foundation

/**
 This public struct describes campus transport.
 */
public struct InterCampusBus: Codable {
    let pickupPoint: String
    let dropOffPoint: String
    let waitingTime: String
}
