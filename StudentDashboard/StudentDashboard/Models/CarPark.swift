//
//  CarPark.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 15/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import Foundation

/**
 This struct describes in campus carpark availibility
 */
public struct CarParkAvailibility: Codable {
    let locationName: String
    let available: Int
}
