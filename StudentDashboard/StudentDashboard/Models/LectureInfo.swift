//
//  LectureInfo.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 15/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import Foundation

/**
 This Lecture info will show every detail of classes
 */
public struct LectureInfo: Codable {
    let lectureName: String
    let lecturerName: String
    let lectureLocation: String
    let startTime: String
    let finishTime: String
}
