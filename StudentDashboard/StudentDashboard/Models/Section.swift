//
//  Section.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 15/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import Foundation

/**
 This public struct map tableview section
 */
public struct InfoSection: Codable {
    let sectionName: String
}
