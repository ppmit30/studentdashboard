//
//  UILable+Extention.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import UIKit

@IBDesignable
class UILabelX: UILabel {
    
    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 10.0
    @IBInspectable var rightInset: CGFloat = 10.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }

    // MARK: - Gradient
       
       @IBInspectable var firstColor: UIColor = UIColor.white {
           didSet {
               updateGradientView()
           }
       }
       
       @IBInspectable var secondColor: UIColor = UIColor.white {
           didSet {
               updateGradientView()
           }
       }
       
       @IBInspectable var horizontalGradient: Bool = false {
           didSet {
               updateGradientView()
           }
       }
       
       override class var layerClass: AnyClass {
           get {
               return CAGradientLayer.self
           }
       }
       
       func updateGradientView() {
           let layer = self.layer as! CAGradientLayer
           layer.colors = [ firstColor.cgColor, secondColor.cgColor ]
           
           if (horizontalGradient) {
               layer.startPoint = CGPoint(x: 1.0, y: 0.5)
               layer.endPoint = CGPoint(x: 0.0, y: 0.5)
           } else {
               layer.startPoint = CGPoint(x: 0, y: 0)
               layer.endPoint = CGPoint(x: 0, y: 1)
           }
       }
}
