//
//  LectureInfoViewModel.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import UIKit

struct StudentDashboardViewModel {
    
    private weak var dataSource : GenericDataSource<StudentDashboard>?
    private weak var service: JSONServiceProtocol?
    
    var onErrorHandling : ((ErrorResult?) -> Void)?
    
    
    /// <#Description#>
    /// - Parameters:
    ///   - service: JSONService converter
    ///   - dataSource: Student Dashboard datasouce
    init(service: JSONServiceProtocol = FileDataService.shared, dataSource : GenericDataSource<StudentDashboard>?) {
        self.dataSource = dataSource
        self.service = service
    }
    
    /**
     This will respond service data to datasource
     */
    public func fetchStudentCampusInfo() {
        
        guard let service = service else {
            onErrorHandling?(ErrorResult.custom(string: "Missing service"))
            return
        }
        
        service.fetchStudentSchedule { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let studentDashboard):
                    self.dataSource?.data.value = [studentDashboard]
                case .failure(let error) :
                    self.onErrorHandling?(error)
                }
            }
        }
    }
}
