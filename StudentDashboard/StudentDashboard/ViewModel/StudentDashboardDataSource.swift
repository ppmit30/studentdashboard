//
//  StudentDashboardDataSource.swift
//  StudentDashboard
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import UIKit

/// Generic DataSource
class GenericDataSource<T> : NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}

final class DashboardDataSource : GenericDataSource<StudentDashboard>, UITableViewDataSource {
    
    // MARK: - Tableview Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.data.value.first?.lectureInfo.count ?? 0
        case 1:
            return self.data.value.first?.carParkAvailibility.count ?? 0
        case 2:
            return self.data.value.first?.interCampusBus.count ?? 0
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.data.value.first?.infoSection.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.lectureInfoCellIdentifier, for: indexPath) as! LectureInfoTableViewCell
            
            cell.getLectureInfo((self.data.value.first?.lectureInfo[indexPath.row])!, And: indexPath.row)
            cell.addShadowToCellInTableView(tableView, at: indexPath)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.carParkAvailibilityIdentifier, for: indexPath) as! CarParkAvailibilityTableViewCell
        cell.updateCarSpaceAvailibility((self.data.value.first?.carParkAvailibility[indexPath.row])!)
            cell.addShadowToCellInTableView(tableView, at: indexPath)
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.transportInfoCellIdentifier, for: indexPath) as! TransportInfoTableViewCell
        cell.updateCellWithCampusTransportDetails((self.data.value.first?.interCampusBus[indexPath.row])!)
            cell.addShadowToCellInTableView(tableView, at: indexPath)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.transportInfoCellIdentifier, for: indexPath) as! TransportInfoTableViewCell
        cell.updateCellWithCampusTransportDetails((self.data.value.first?.interCampusBus[indexPath.row])!)
            cell.addShadowToCellInTableView(tableView, at: indexPath)
            return cell
        }
    }
}

// MARK: - UITableviewDelegate
extension DashboardDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                return CGFloat(Constants.lectureInfoCellHeight)
            } else {
                return CGFloat(Constants.lectureInfoCellWithoutView)
            }
        default:
            return CGFloat(Constants.regulareCellHeight)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(Constants.heightForHeaderInSection)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(Constants.heightForFooterInSection)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.sectionHeaderIdentifier) as! HeaderViewTableViewCell
        
        if section != 0 {
            cell.setSectionName((self.data.value.first?.infoSection[section])!)
        }
        return cell
    }
}
