//
//  DashboardDataSourceTests.swift
//  StudentDashboardTests
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import XCTest
@testable import StudentDashboard

class DashboardDataSourceTests: XCTestCase {
    
    var dataSource: DashboardDataSource!

    override func setUp() {
        super.setUp()
        dataSource = DashboardDataSource()
    }

    override func tearDown() {
        dataSource = nil
        super.tearDown()
    }

    func testEmptyValueInDataSource() {
        
        // giving empty data value
        dataSource.data.value = []
        
        let tableView = UITableView()
        tableView.dataSource = dataSource
        
        // expected one section
        XCTAssertEqual(dataSource.numberOfSections(in: tableView), 1, "Expected one section in table view")
        
        // expected zero cells
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 0, "Expected no cell in table view")
    }
    
    
}
