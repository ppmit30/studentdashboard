//
//  StudentClassesTests.swift
//  StudentDashboardTests
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import XCTest
@testable import StudentDashboard

class StudentClassesTests: XCTestCase {

    var dataSource : GenericDataSource<StudentDashboard>?

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    // JSON File found
    func testJsonFileFound() throws {
        guard let url = Bundle.main.path(forResource: "StudentDashboard", ofType: "json") else {
            XCTFail("Missing file: StudentDashboard.json")
            return
        }
        let theFileName = (url as NSString).lastPathComponent
        XCTAssertEqual("StudentDashboard.json", theFileName, "Found File")
    }
    
    func testJSONDataNotEmptyCheck() throws {
        FileDataService.shared.fetchStudentSchedule { [weak self] studentInfo in
            guard let self = self else { return }
            //Check Array is not Empty.
            XCTAssertFalse((self.dataSource?.data.value.first?.lectureInfo.count ?? 0)  == 0, "Array can not be empty!!")
            XCTAssertFalse((self.dataSource?.data.value.first?.carParkAvailibility.count ?? 0)  == 0, "Array can not be empty!!")
            XCTAssertFalse((self.dataSource?.data.value.first?.interCampusBus.count ?? 0)  == 0, "Array can not be empty!!")
        }
    }
    
    func testJSONMapping() throws {
            FileDataService.shared.fetchStudentSchedule { [weak self] studentInfo in
            guard let self = self else { return }

                //Check Array in JSON.
                XCTAssertTrue(self.dataSource?.data.value.first?.lectureInfo.count  == 3, "As per the degsin Lecture shoude be 3 items." );
                XCTAssertTrue(self.dataSource?.data.value.first?.carParkAvailibility.count == 1, "As per the degsin Car park Availibility shoude be 1 items." );
                XCTAssertTrue(self.dataSource?.data.value.first?.interCampusBus.count == 2, "As per the degsin Inter Campus Bus shoude be 2 items." );

                // Lecture Inforramtion JSON Test
                let lectureInfo = self.dataSource?.data.value.first?.lectureInfo[0]
                XCTAssertEqual(lectureInfo!.lectureName, "FIT1031 Lecture 01")
                XCTAssertEqual(lectureInfo!.lecturerName, "Arun Kongaurthu")
                XCTAssertEqual(lectureInfo!.lectureLocation, "S4, 13 College Walk, Clayton")
                XCTAssertEqual(lectureInfo!.startTime, "8:00 AM")
                XCTAssertEqual(lectureInfo!.finishTime, "10:00 AM")

                // Car Park Availibility
                let carParkAvailibility = self.dataSource?.data.value.first?.carParkAvailibility[0]
                XCTAssertEqual(carParkAvailibility!.locationName, "Clayton life feed")
                XCTAssertEqual(carParkAvailibility!.available, 645)

                // Car Park Availibility
                let interCampusBus = self.dataSource?.data.value.first?.interCampusBus[0]
                XCTAssertEqual(interCampusBus!.pickupPoint, "Clayton")
                XCTAssertEqual(interCampusBus!.dropOffPoint, "Caulfield")
                XCTAssertEqual(interCampusBus!.waitingTime, "4 mins")
            }
        }
}
