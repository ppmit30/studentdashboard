//
//  StudentDashboardViewModelTests.swift
//  StudentDashboardTests
//
//  Created by Pratik Bhaliya on 16/3/20.
//  Copyright © 2020 Pratik Bhaliya. All rights reserved.
//

import XCTest
@testable import StudentDashboard

class StudentDashboardViewModelTests: XCTestCase {

    var viewModel: StudentDashboardViewModel!
    var dataSource: GenericDataSource<StudentDashboard>!
    fileprivate var service: MockStudentDashboardService!
    
    override func setUp() {
        super.setUp()
        self.service = MockStudentDashboardService()
        self.dataSource = GenericDataSource<StudentDashboard>()
        self.viewModel = StudentDashboardViewModel(service: service, dataSource: dataSource)
    }

    override func tearDown() {
        self.viewModel = nil
        self.dataSource = nil
        self.service = nil
        super.tearDown()
    }

    func testFetchWithNoService() {
        let expectation = XCTestExpectation(description: "No service")
        
        // giving no service to a view model
        service = nil
        
        // expected to not be able to fetch currencies
        viewModel.onErrorHandling = { error in
            expectation.fulfill()
        }
        
        viewModel.fetchStudentCampusInfo()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testFetchStudentDashInfo() {
        
        let expectation = XCTestExpectation(description: "Dashboard info fetch")
        
        // giving a service mocking currencies
        service.studentDashboard?.lectureInfo = [LectureInfo(lectureName: "FIT 1031 Lecture 01", lecturerName: "Arun Kongaurthu", lectureLocation: "S4, 13 Collage Walk, Clayton", startTime: "8:00 AM", finishTime: "10:00 AM")]
        
        viewModel.onErrorHandling = { _ in
            XCTAssert(false, "ViewModel should not be able to fetch without service")
        }
        
        dataSource.data.addObserver(self) { _ in
            expectation.fulfill()
        }
        
        viewModel.fetchStudentCampusInfo()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testFetchNoSchedule() {
        
        let expectation = XCTestExpectation(description: "No schedule")
        
        // giving a service mocking error during fetching currencies
        service.studentDashboard = nil
        
        // expected completion to fail
        viewModel.onErrorHandling = { error in
            expectation.fulfill()
        }
        
        viewModel.fetchStudentCampusInfo()
        wait(for: [expectation], timeout: 5.0)
    }
}

fileprivate class MockStudentDashboardService : JSONServiceProtocol {
    var studentDashboard : StudentDashboard?
    
    func fetchStudentSchedule(_ completion: @escaping ((Result<StudentDashboard, ErrorResult>) -> Void)) {
        if let dashboardInfo = studentDashboard {
            completion(Result.success(dashboardInfo))
        } else {
            completion(Result.failure(ErrorResult.custom(string: "No Schedules")))
        }
    }
}
